package br.com.senac.sysagenda.dao;

import java.util.List;
import javax.persistence.EntityManager;

public abstract class DAO<T> {

    protected EntityManager em;

    private final Class<T> entidade;

    public DAO(Class<T> entidade) {
        this.entidade = entidade;
    }

    public void save(T entity) { //Salva
        
        this.em = JPAUtil.getEntityManager(); //Abrir conexao 
        em.getTransaction().begin(); //Comecei uma transacao
        em.persist(entity); //Inserir no banco
        em.getTransaction().commit(); //Fechar uma trasacao
        em.close(); //Fechar conexao

    }

    public void update(T entity) { //Atualiza
        
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(entity); //Atualizar
        em.getTransaction().commit();
        em.close();

    }

    public void delete(T entity) {
        
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.remove(em.merge(entity)); 
        em.getTransaction().commit();
        em.close();
        
    }

    public T find(long id) {
        
        this.em = JPAUtil.getEntityManager(); 
        em.getTransaction().begin();
        T t = em.find(entidade, id);
        em.getTransaction().commit();
        em.close();
        
        return t;
    }

    public List<T> findAll() {
        
        this.em = JPAUtil.getEntityManager();
        List<T> lista;
        em.getTransaction().begin();
        lista = em.createQuery("from " + entidade.getName() + " e" ).getResultList() ;
        em.getTransaction().commit();
        em.close();
                
        return lista;
    }
        
}
