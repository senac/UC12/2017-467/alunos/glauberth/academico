package br.com.senac.sysagenda.dao;

import br.com.senac.sysagenda.model.Estado;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// Criando conexão com o banco de dados!
public class JPAUtil {

    private static EntityManagerFactory enf = Persistence.createEntityManagerFactory("SysAgendaPU");

    public static EntityManager getEntityManager() {

        try {

            return enf.createEntityManager();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Erro ao acessar o banco de dados!");
        }

    }
   
    /*
    public static void main(String[] args) {

        Estado estado = new Estado();
        estado.setNome("Minas Gerais");
        estado.setSigla("MG");

        Estado estado1 = new Estado();
        estado1.setNome("Rio de Janeiro");
        estado1.setSigla("RJ");

        EstadoDAO dao = new EstadoDAO();
        dao.save(estado);
        dao.save(estado1);

        List<Estado> lista = dao.findAll();

        for (Estado e : lista) {
            System.out.println(e.getNome());

        }

    }
    */
}
