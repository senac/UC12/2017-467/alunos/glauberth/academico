
package br.com.senac.sysagenda.bean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

@Named(value = "helloBean")
@RequestScoped
public class HelloBean {
    
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }    
    
    public HelloBean() {
    }
    
    public String vai(){
        return "index";
    }
    
        
    
}
