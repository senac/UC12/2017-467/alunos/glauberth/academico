package br.com.senac.sysagenda.bean;

import br.com.senac.sysagenda.dao.EstadoDAO;
import br.com.senac.sysagenda.model.Estado;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "estadoBean")
@ViewScoped
public class EstadoBean extends Bean implements Serializable {

    private Estado estado;
    private EstadoDAO dao;

    public EstadoBean() {
        this.estado = new Estado();
        this.dao = new EstadoDAO();
    }

    public void salvar() {

        if (this.estado.getId() == 0) {

            dao.save(estado);
            addMessageInfo("Estado salvo com Sucesso!");

        } else {
            dao.update(estado);
            addMessageInfo("Alterado com sucesso!");
        }
    }

    public void novo() {
        this.estado = new Estado();
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

}
