
package br.com.senac.sysagenda.dao;

import br.com.senac.sysagenda.model.Estado;


public class EstadoDAO extends DAO<Estado> {
    
    public EstadoDAO() {
        super(Estado.class);
    }
    
}
