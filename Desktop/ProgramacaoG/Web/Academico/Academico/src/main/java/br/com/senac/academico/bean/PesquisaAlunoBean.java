package br.com.senac.academico.bean;

import br.com.senac.academico.dao.AlunoDAO;
import br.com.senac.academico.model.Aluno;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "pesquisaAlunoBean")
@ViewScoped
public class PesquisaAlunoBean implements Serializable {

    private Aluno alunoSelecionado;
    private AlunoDAO dao;
    private List lista;

    private String nome;
    private String codigo;

    public PesquisaAlunoBean() {
    }

    @PostConstruct
    public void inity() {

        this.alunoSelecionado = new Aluno();
        this.dao = new AlunoDAO();
        lista = dao.findAll();

    }

    public void pesquisar() {

        try {
            this.lista = this.dao.findByFiltro(codigo, nome);
        } catch (Exception e) {
        }

    }
    
    public void salvar(){
        
        if(this.alunoSelecionado.getId() == 0){
            this.dao.save(alunoSelecionado);            
        }else{
            this.dao.update(alunoSelecionado);
        }
        
        this.pesquisar();
        
    }
    
    public void deletar(Aluno aluno){
        
        try {
            this.dao.delete(alunoSelecionado);
        } catch (Exception e) {
        }
       
        this.pesquisar();
        
    }

    public Aluno getAlunoSelecionado() {
        return alunoSelecionado;
    }

    public void setAlunoSelecionado(Aluno alunoSelecionado) {
        this.alunoSelecionado = alunoSelecionado;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    public List getLista() {
        return lista;
    }

    public void setLista(List lista) {
        this.lista = lista;
    }

}
