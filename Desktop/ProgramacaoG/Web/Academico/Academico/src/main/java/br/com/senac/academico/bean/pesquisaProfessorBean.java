
package br.com.senac.academico.bean;

import br.com.senac.academico.dao.ProfessorDAO;
import br.com.senac.academico.model.Professor;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "pesquisaProfessorBean")
@ViewScoped
public class pesquisaProfessorBean implements Serializable{
    
    private Professor professor;
    private ProfessorDAO dao;
    private List lista;    
    

    public pesquisaProfessorBean() {
    }
    
    @PostConstruct
    public void inity(){
        this.professor = new Professor();
        this.dao = new ProfessorDAO();
        lista = dao.findAll();
    }   
    
    
    
}
